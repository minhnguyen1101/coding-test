﻿create table [Address]
(
  id int identity primary key,
  street nvarchar(100) not null,
  ward nvarchar(100) not null,
  district nvarchar(100) not null,
  city nvarchar(100) not null,
  country nvarchar(100) not null,

  lat decimal(20, 14) not null,
  lng decimal(20, 14) not null,

  rowVersion timestamp not null   -- protects the unlikely case where a change is submitted and validated concurrently of a single edit/submit.
);

GO 

insert into [Address] (street, ward, district, city, country, lat, lng)
values ('72 Le Thanh Ton', 'Ben Nghe ward', 'District 1', 'Ho Chi Minh city', 'Vietnam', 10.7777451, 106.70176839999999);

GO

insert into [Address] (street, ward, district, city, country, lat, lng)
values ('138 Hai Ba Trung', 'Da Kao ward', 'District 1', 'Ho Chi Minh city', 'Vietnam', 10.784277, 106.69685200000004);