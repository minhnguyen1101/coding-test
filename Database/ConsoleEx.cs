using System;
using System.Diagnostics;

namespace CodingTest.Database
{
  static class ConsoleEx
  {
    public static void PrintKeyValue(string key, string value)
    {
      Console.Write(key);
      Console.ForegroundColor = ConsoleColor.White;
      Console.WriteLine(value);
      Console.ResetColor();
    }

    private static void PrintInColor(string message, ConsoleColor color, TimeSpan elapsed)
    {
      Console.ForegroundColor = color;
      Console.Write(message);
      Console.ForegroundColor = ConsoleColor.DarkCyan;
      Console.WriteLine($@" [{elapsed.TotalSeconds :00.0000 s}]");
      Console.ResetColor();
    }

    public static void PrintOK(string message = "OK", TimeSpan elapsed = default(TimeSpan))
    {
      PrintInColor(message, ConsoleColor.Green, elapsed);
    }

    public static void PrintNOK(string message = "Error", TimeSpan elapsed = default(TimeSpan))
    {
      PrintInColor(message, ConsoleColor.Red, elapsed);
    }

    public static void LogStep(string name, Action step)
    {
      Console.Write(name + "...");
      Console.SetCursorPosition(50, Console.CursorTop);
      var stopwatch = new Stopwatch();
      stopwatch.Start();
      try
      {
        step();
        stopwatch.Stop();
        PrintOK(elapsed: stopwatch.Elapsed);
      }
      catch
      {
        stopwatch.Stop();
        PrintNOK(elapsed: stopwatch.Elapsed);
        throw;
      }
    }

    public static ConsoleStep WriteStep(string name)
    {
      Console.Write(name + "...");
      Console.SetCursorPosition(50, Console.CursorTop);
      return new ConsoleStep();
    }

    public class ConsoleStep : IDisposable
    {
      private bool ok = false;
      private Stopwatch stopwatch = new Stopwatch();

      public ConsoleStep()
      {
        stopwatch.Start();
      }

      public void Done()
      {
        stopwatch.Stop();
        PrintInColor("OK", ConsoleColor.Green, stopwatch.Elapsed);
        ok = true;
      }

      void IDisposable.Dispose()
      {
        if (ok) return;
        stopwatch.Stop();
        PrintInColor("Error", ConsoleColor.Red, stopwatch.Elapsed);
        ok = true;
      }
    }
  }
}
