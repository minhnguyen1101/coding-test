﻿using System;
using System.Data.SqlClient;

namespace CodingTest.Database
{
  public static class SqlServer
  {
    public static SqlConnection Open(string connectionString)
    {
      var connection = new SqlConnection(connectionString);
      connection.Open();
      return connection;
    }

    public static void Execute(this SqlConnection connection, string sql)
    {
      using (var cmd = new SqlCommand(sql, connection))
        cmd.ExecuteNonQuery();
    }

    public static T QueryScalar<T>(this SqlConnection connection, string sql)
    {
      using (var cmd = new SqlCommand(sql, connection))
      {
        object result = cmd.ExecuteScalar();
        return result is DBNull ? default(T) : (T)Convert.ChangeType(result, typeof(T));
      }
    }
  }
}
