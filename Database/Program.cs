﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace CodingTest.Database
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().Exec(args);
        }

        public void Exec(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine("Database update tool");
            Console.WriteLine("====================");

            string command = args.Length > 0 ? args[0] : "info";
            var knownCommands = new[] { "up", "info", "clean", "rebuild" };
            if (!knownCommands.Contains(command))
            {
                Console.WriteLine($"Unknown command: {command}.\nSupported commands are {string.Join(", ", knownCommands)}.");
                return;
            }

            var config = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            string connectionString = config["Data:MainDb"];
            Console.WriteLine("DB settings:");
            var cs = new SqlConnectionStringBuilder(connectionString);
            // Make sure we use the user's account to connect to DB.
            // The login/password from application typically doesn't have DDL rights.
            cs.Remove("User Id");
            cs.Remove("Password");
            cs.IntegratedSecurity = true;
            ConsoleEx.PrintKeyValue("    Server: ", cs.DataSource);
            ConsoleEx.PrintKeyValue("    Database: ", cs.InitialCatalog);

            if (command != "info")
            {
                Console.Write("Continue ? (y/n) ");
                var confirm = Console.ReadLine();
                if (confirm.Trim().ToLower() != "y")
                {
                    Console.WriteLine("Operation canceled");
                    return;
                }
            }

            using (var connection = SqlServer.Open(cs.ToString()))
            {
                switch (command)
                {
                    case "info":
                        GetCurrentDBVersion(connection);
                        return;
                    case "clean":
                        Clean(connection);
                        return;
                    case "up":
                        Up(connection);
                        return;
                    case "rebuild":
                        Clean(connection);
                        Up(connection);
                        return;
                    default:
                        Console.WriteLine("Unknown command: " + command);
                        return;
                }
            }
        }

        private void Up(SqlConnection connection)
        {
            string dbVersion = GetCurrentDBVersion(connection);

            using (var md5 = MD5.Create())
            {
                var allFiles = from file in Directory.EnumerateFiles("Sql", "*.sql", SearchOption.AllDirectories)
                               let filename = Path.GetFileName(file)
                               let version = filename.Substring(0, filename.IndexOf('-'))
                               where string.CompareOrdinal(dbVersion, version) < 0
                               orderby filename
                               select new
                               {
                                   File = file,
                                   Name = filename,
                                   Version = version
                               };
                foreach (var file in allFiles)
                {
                    using (var step = ConsoleEx.WriteStep("Applying " + file.Name))
                    {
                        var text = File.ReadAllText(file.File);
                        var blocks = Regex.Split(text, @"^\s*GO\s*$", RegexOptions.Multiline);
                        foreach (var sql in blocks)
                            connection.Execute(sql);

                        string description = Path.GetFileNameWithoutExtension(file.File).Substring(file.Version.Length + 1);
                        string hash = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(text))).Replace("-", "");
                        // TODO: parameters
                        connection.Execute($@"insert into __Version values ('{file.Version}', '{description}', '{hash}', current_timestamp)");

                        step.Done();
                    }
                }
            }

            string appVersion = typeof(Program).GetTypeInfo()
                                               .Assembly
                                               .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
                                               .InformationalVersion;
            connection.Execute("exec sp_updateextendedproperty 'ToolVersion', '" + appVersion + "', 'schema', 'dbo', 'table', '__Version'");

            Console.Write("Deployed version ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(appVersion);
            Console.ResetColor();
        }

        private void Clean(SqlConnection connection)
        {
            ConsoleEx.LogStep("Cleaning schema", () =>
                connection.Execute(@"declare @drop nvarchar(max) = '';
                               select @drop = @drop + ';alter table [' + object_name(parent_object_id) + '] drop constraint ' + name from sys.foreign_keys;
                               select @drop = @drop + ';drop function [' + name + ']' from sys.objects where type in (N'FN', N'IF', N'TF');
                               select @drop = @drop + ';drop procedure [' + name + ']' from sys.procedures;
                               select @drop = @drop + ';drop view [' + name + ']' from sys.views;
                               select @drop = @drop + ';drop table [' + name + ']' from sys.tables;
                               select @drop = @drop + ';drop sequence ' + name from sys.sequences;
                               exec sp_executesql @drop;"));
        }

        private string GetCurrentDBVersion(SqlConnection connection)
        {
            string dbVersion, appVersion;
            using (var step = ConsoleEx.WriteStep("Determining DB version"))
            {
                bool tableExists = connection.QueryScalar<int>("select count(*) from sys.tables where name = '__Version'") > 0;
                if (!tableExists)
                {
                    connection.Execute(@"create table __Version (version varchar(20) primary key, description nvarchar(100) null, md5 char(32) not null, installed smalldatetime not null)");
                    connection.Execute("exec sp_addextendedproperty 'ToolVersion', 'none', 'schema', 'dbo', 'table', '__Version'");
                }
                dbVersion = connection.QueryScalar<string>("select max(version) from __Version") ?? "00.00.000";
                appVersion = connection.QueryScalar<string>("select value from sys.fn_listextendedproperty('ToolVersion', 'schema', 'dbo', 'table', '__Version', null, null)") ?? "none";
                step.Done();
            }

            Console.WriteLine("Last SQL script is " + dbVersion);
            Console.Write("Database version is ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(appVersion);
            Console.ResetColor();

            return dbVersion;
        }
    }
}
