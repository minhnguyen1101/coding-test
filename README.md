# README #

This README would normally document whatever steps are necessary to get your application up and running.

How to run application?
Firstly you need to run the command line 'npm run buildvendor' to combine all frameworks for this website into vendor-manifest.json
Secondly, you need to run the following command line 'npm run build' to combine all the ts,tsx,ess files
Thirdly, you need to create an empty database in sql server and then go into folder Database to run command line 'dotnet run rebuild' to init table and data
Note: You need to set connectionstring in appsettings.json within both projects. 
I created this website in only about 30hrs so there may be some small imperfections but I have implemented all your the requirements.
I am not completely satisfied with the layout but I hope it is enough for you to see my capabilities and enjoy the website.