﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CodingTest.Model;

namespace CodingTest.Data
{
    [DbConfigurationType(typeof(AppDbConfig))]
    public class AppDbContext : DbContext
    {
        public DbSet<Address> Addresss { get; set; }

        public AppDbContext() : base(AppDbConfig.ConnectionString)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.UseDatabaseNullSemantics = true;
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Conventions.Remove<PluralizingTableNameConvention>();
            builder.Conventions.Add(new RowVersionConvention());

            builder.Entity<Address>().Property(x => x.Lat).HasPrecision(20, 14);
            builder.Entity<Address>().Property(x => x.Lng).HasPrecision(20, 14);
        }
    }
}
