﻿using System.Data.Entity.ModelConfiguration.Conventions;

namespace CodingTest.Data
{
    public class RowVersionConvention : Convention
    {
        public RowVersionConvention()
        {
            Properties<byte[]>()
              .Where(p => p.Name == "RowVersion")
              .Configure(c => c.IsRowVersion());
        }
    }
}
