import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { HttpClientProvider } from './components/httpClientProvider';

import { routes } from './routes';

ReactDOM.render(
    <HttpClientProvider>
        <Router routes={routes} history={browserHistory} />
    </HttpClientProvider>,
    document.getElementById('react-app')
);