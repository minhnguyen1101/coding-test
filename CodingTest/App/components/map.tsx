import * as React from 'react'
import * as ReactDOM from 'react-dom'

export class Map extends React.Component<any, any> {
    map: google.maps.Map;
    geocoder: google.maps.Geocoder;
    infoWindow: google.maps.InfoWindow;
    marker: google.maps.Marker;
    autocomplete: google.maps.places.Autocomplete;

    componentDidMount() {
        const { lat, lng } = this.props.initialPosition;
        this.geocoder = new google.maps.Geocoder();
        this.infoWindow = new google.maps.InfoWindow();
        this.marker = new google.maps.Marker();

        this.map = new google.maps.Map(this.refs.map, {
            center: { lat, lng },
            zoom: 16,
            mapMaker: true
        });
        this.map.addListener('click', this.addMarker.bind(this));

        let card = document.getElementById('pac-card');
        this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(card);

        var input = document.getElementById('pac-input') as HTMLInputElement;
        this.autocomplete = new google.maps.places.Autocomplete(input);
        this.autocomplete.bindTo('bounds', this.map);
        this.autocomplete.addListener('place_changed', this.autocompleteChanged.bind(this));

        this.addMarker({ lat, lng }, true);
    }

    addMarker(latLng, isFirstLoad = false) {
        let self = this;

        self.infoWindow.close();
        self.marker.setMap(null);

        let {lat, lng} = latLng;
        if (!isFirstLoad) {
            lat = latLng.latLng.lat();
            lng = latLng.latLng.lng();
        }
        this.geocoder.geocode({ location: { lat, lng } }, (results: any, status: any) => {
            if (status === 'OK') {
                if (results[0]) {
                    self.props.data.lat = lat;
                    self.props.data.lng = lng;
                    self.setData(results[0], self.props.data);
                    self.showInformation(self, results[0]);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    autocompleteChanged() {
        let self = this;

        self.infoWindow.close();
        self.marker.setMap(null);

        var place = self.autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        if (place.geometry.viewport) {
            self.map.fitBounds(place.geometry.viewport);
        } else {
            self.map.setCenter(place.geometry.location);
            self.map.setZoom(18);
        }

        self.props.data.lat = place.geometry.location.lat();
        self.props.data.lng = place.geometry.location.lng();
        self.setData(place, self.props.data);
        self.showInformation(self, place);
    }

    showInformation(self, location) {
        self.map.setZoom(18);
        self.marker.setOptions({
            position: { lat: self.props.data.lat, lng: self.props.data.lng },
            map: self.map
        });
        self.marker.setAnimation(google.maps.Animation.BOUNCE);

        self.infoWindow.setContent(location.formatted_address);
        self.infoWindow.open(self.map, self.marker);

        setTimeout(() => {
            self.marker.setAnimation(null);
        }, 5000);
    }

    setData(result, data) {
        if (result.address_components.length > 5) {
            data.street = `${result.address_components[0].long_name} ${result.address_components[1].long_name}`;
            data.ward = result.address_components[2].long_name;
            data.district = result.address_components.length > 6 ? result.address_components[4].long_name : result.address_components[3].long_name;
            data.city = result.address_components.length > 6 ? result.address_components[3].long_name : result.address_components[4].long_name;
            data.country = result.address_components[result.address_components.length - 1].long_name;
        } else {
            data.street = result.address_components[0].long_name;
            data.ward = result.address_components[1].long_name;
            data.district = result.address_components[2].long_name;
            data.city = result.address_components[3].long_name;
            data.country = result.address_components[4].long_name;
        }
    }

    render() {
        const mapStyle = {
            width: 1140,
            height: 600
        };

        return <div>
            <div className='pac-card col-xs-5' id='pac-card'>
                <div className='title'>
                    Autocomplete search
                </div>
                <div className='form-group'>
                    <input id="pac-input" className='form-control' placeholder='Enter a location' />
                </div>
            </div>
            <div ref='map' style={mapStyle}></div>
        </div>;
    }
}