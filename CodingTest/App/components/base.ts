﻿import * as React from 'react';
import { HttpClientAPI } from './httpClientProvider';


export const BaseContextTypes = {
    httpClient: React.PropTypes.object,
    router: React.PropTypes.object,
};

export abstract class BaseComponent<P, S> extends React.Component<P, S> {
    static contextTypes = BaseContextTypes;
        
    get httpClient(): HttpClientAPI {
        return this.context["httpClient"] as HttpClientAPI;
    }
}