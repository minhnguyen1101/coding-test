import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import { BaseComponent } from '../components/base';
import { Map } from '../components/map';
import { Address } from '../model/address';

export class Form extends BaseComponent<any, any> {
    constructor(props: any, ctx) {
        super(props, ctx);
    }

    normalForm() {
        return (
            <div>
                <div className='col-xs-6'>
                    <div className='form-group'>
                        <label>Street</label>
                        <input className='form-control' value={this.props.data.street} fullWidth={true}
                            onChange={(evt) => { this.props.data.street = (evt.target as HTMLInputElement).value; this.setState(this.state) } } />
                    </div>
                    <div className='form-group'>
                        <label>Ward</label>
                        <input className='form-control' value={this.props.data.ward} fullWidth={true}
                            onChange={(evt) => { this.props.data.ward = (evt.target as HTMLInputElement).value; this.setState(this.state) } } />
                    </div>
                    <div className='form-group'>
                        <label>District</label>
                        <input className='form-control' value={this.props.data.district} fullWidth={true}
                            onChange={(evt) => { this.props.data.district = (evt.target as HTMLInputElement).value; this.setState(this.state) } } />
                    </div>
                </div>
                <div className='col-xs-6'>
                    <div className='form-group'>
                        <label>City</label>
                        <input className='form-control' value={this.props.data.city} fullWidth={true}
                            onChange={(evt) => { this.props.data.city = (evt.target as HTMLInputElement).value; this.setState(this.state) } } />
                    </div>
                    <div className='form-group'>
                        <label>Country</label>
                        <input className='form-control' value={this.props.data.country} fullWidth={true}
                            onChange={(evt) => { this.props.data.country = (evt.target as HTMLInputElement).value; this.setState(this.state) } } />
                    </div>
                </div>
            </div>
        );
    }

    googleMapForm() {
        return <Map isNew={this.props.data.id != 0} initialPosition={{ lat: this.props.data.lat, lng: this.props.data.lng }} data={this.props.data} />;
    }

    render() {
        return (
            <div className='col-xs-12'>
                {this.props.option === '1' ? this.normalForm() :
                    this.props.option === '2' ? this.googleMapForm() : null}
            </div>
        );
    }
}