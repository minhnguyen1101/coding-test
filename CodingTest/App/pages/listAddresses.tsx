import * as React from 'react';
import * as Api from '../services/address';
import { browserHistory } from 'react-router';
import { Table, Column, Cell } from 'fixed-data-table';
import { BaseComponent } from '../components/base';
import { Address } from '../model/address';

export class ListAddresses extends BaseComponent<any, any> {
    constructor(props: any, ctx) {
        super(props, ctx);
        this.state = {
            addresses: [],
            filteredDataList: [],
            sortBy: 'city',
            sortDir: null
        };
    }

    async componentDidMount() {
        const results = await Api.getAll(this.httpClient);
        this.state.addresses = results;
        this.state.filteredDataList = this.state.addresses.slice();
        this.setState(this.state);
    }

    addNew() {
        browserHistory.push('/address');
    }

    exportToCsv() {
        Api.exportToCsv(this.httpClient);
    }
       
    renderHeader(label, cellDataKey) {
        return <div>
            <a onClick={this.sortRowsBy.bind(this, cellDataKey)}>{label}</a>
        </div>;
    }

    sortRowsBy(cellDataKey) {
        var sortDir = this.state.sortDir;
        var sortBy = cellDataKey;
        if (sortBy === this.state.sortBy) {
            sortDir = this.state.sortDir === 'ASC' ? 'DESC' : 'ASC';
        } else {
            sortDir = 'DESC';
        }
        var rows = this.state.addresses.slice();
        rows.sort((a, b) => {
            var sortVal = 0;
            if (a[sortBy] > b[sortBy]) {
                sortVal = 1;
            }
            if (a[sortBy] < b[sortBy]) {
                sortVal = -1;
            }

            if (sortDir === 'DESC') {
                sortVal = sortVal * -1;
            }
            return sortVal;
        });

        this.setState({ sortBy, sortDir, filteredDataList: rows });
    }

    rowClick(event, index) {
        browserHistory.push(`/address/${this.state.filteredDataList[index].id}`);
    }

    render() {
        var sortDirArrow = '';
        if (this.state.sortDir !== null) {
            sortDirArrow = this.state.sortDir === 'DESC' ? ' ?' : ' ?';
        }

        return (
            <div>
                <div className='pull-right margin-bottom-20'>
                    <button className='btn btn-primary' onClick={this.exportToCsv.bind(this)}>Export to CSV</button>
                    <button className='btn btn-success margin-left-20' onClick={this.addNew}>Add new</button>
                </div>
                <Table
                    onRowClick={this.rowClick.bind(this)}
                    height={40 + ((this.state.filteredDataList.length + 1) * 30)}
                    width={1140}
                    rowsCount={this.state.filteredDataList.length}
                    rowHeight={30}
                    headerHeight={30}
                    rowGetter={function (rowIndex) { return this.state.filteredDataList[rowIndex]; }.bind(this)}>
                    <Column dataKey="id" width={40} label={'Id' + (this.state.sortBy === 'id' ? sortDirArrow : '')} headerRenderer={this.renderHeader.bind(this)}/>
                    <Column dataKey="street" width={300} label={'Street' + (this.state.sortBy === 'street' ? sortDirArrow : '')} headerRenderer={this.renderHeader.bind(this)} />
                    <Column dataKey="ward" width={200} label={'Ward' + (this.state.sortBy === 'ward' ? sortDirArrow : '')} headerRenderer={this.renderHeader.bind(this)} />
                    <Column dataKey="district" width={200} label={'District' + (this.state.sortBy === 'district' ? sortDirArrow : '')} headerRenderer={this.renderHeader.bind(this)}/>
                    <Column dataKey="city" width={200} label={'City' + (this.state.sortBy === 'city' ? sortDirArrow : '')} headerRenderer={this.renderHeader.bind(this)} />
                    <Column dataKey="country" width={200} label={'Country' + (this.state.sortBy === 'country' ? sortDirArrow : '')} headerRenderer={this.renderHeader.bind(this)} />
                </Table>
            </div>
        );
    }
}