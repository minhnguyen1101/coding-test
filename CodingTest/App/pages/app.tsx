import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Link } from 'react-router';

export class App extends React.Component<any, any> {
    render() {
        return (
            <div>
                <nav className='navbar navbar-default'>
                    <div class='container-fluid'>
                        <div className='collapse navbar-collapse'>
                            <ul className='nav navbar-nav'>
                                <li><Link to='/' onlyActiveOnIndex activeClassName='active' >Home</Link></li>
                                <li><Link to='/addresses' >Addresses</Link></li>

                            </ul>
                        </div>                        
                    </div>                    
                </nav>
                <div className='container'>
                    {this.props.children}
                </div>
            </div>
        );
    }
}