import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Api from '../services/address';
import { browserHistory } from 'react-router';
import { BaseComponent } from '../components/base';
import { Form } from '../components/form';

export class Address extends BaseComponent<any, any> {
    isNew = false;

    constructor(props: any, ctx) {
        super(props, ctx);
        this.state = {
            selectedOption: 0,
            address: {}
        };
    }

    async componentDidMount() {
        this.isNew = !this.props.params.hasOwnProperty('id')
        if (this.isNew) {
            this
            this.state.address.id = 0;
            this.getCurrentLocation();
            return;
        }

        const result = await Api.load(this.httpClient, this.props.params.id);
        this.state.address = result;
        this.state.selectedOption = '1';
        this.setState(this.state);
    }

    getCurrentLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.setCurrentLocation.bind(this));
        } else {
            console.log('Geolocation is not supported by this browser.');
        }
    }

    setCurrentLocation(position: Position) {
        this.state.address.lat = position.coords.latitude;
        this.state.address.lng = position.coords.longitude;
        this.setState(this.state);
    }

    back() {
        browserHistory.goBack();
    }

    async save() {
        const result = await Api.save(this.httpClient, this.state.address);
        if (!result) {
            alert('Save failed!');
            return
        }
        this.back();
    }

    async delete() {
        await Api.remove(this.httpClient, this.state.address.id, this.state.address.rowVersion);       
        this.back;
    }

    handleOptionChange(changeEvent) {
        this.setState({
            selectedOption: changeEvent.target.value
        });
    }

    render() {
        return (
            <div>
                <h1>Address's detail</h1>
                <div className='row text-center margin-bottom-20'>
                    <div className='col-xs-2 col-xs-offset-4'>
                        <label>
                            <input type="radio" className='margin-right-10' value="1"
                                checked={this.state.selectedOption === '1'} onClick={this.handleOptionChange.bind(this)} />
                            Normal form
                        </label>
                    </div>
                    <div className='col-xs-2'>
                        <label>
                            <input type="radio" className='margin-right-10' value="2"
                                checked={this.state.selectedOption === '2'} onClick={this.handleOptionChange.bind(this)} />
                            Google map
                        </label>
                    </div>
                </div>
                <div className='row margin-bottom-20'>
                    <Form option={this.state.selectedOption} data={this.state.address} />
                </div>
                <div className='row'>
                    <button className='btn btn-default margin-left-15' onClick={this.back}>Back</button>
                    {!this.isNew ? <button className='btn btn-danger margin-left-20' onClick={this.delete.bind(this)}>Delete</button> : ''}
                    {this.state.selectedOption !== 0 ? <button className='btn btn-warning margin-left-20' onClick={this.save.bind(this)}>Save</button> : ''}
                </div>
            </div>
        )
    }
}