import * as React from 'react';
import '../content/css/site.less';


export interface IHomeProps { compiler: string; framework: string; }

export class Home extends React.Component<IHomeProps, {}> {
    render() {
        return <div>
            <h1>Hello everyone, this is my coding test!</h1>

            <a href='https://gist.github.com/hoanglamhuynh/49d1c68442442273ab94b0b6073002ed'>Requirements</a>
        </div>;
    }
}