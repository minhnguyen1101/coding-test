import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';

import { App } from './pages/app';
import { Home } from './pages/home';
import { ListAddresses } from './pages/listAddresses';
import { Address } from './pages/address';

export const routes = (
    <Router history={browserHistory}>
        <Route path="/" component={App as any}>
            <IndexRoute component={Home as any} />
            <Route path="addresses" component={ListAddresses as any} />
            <Route path="address" component={Address as any} />
            <Route path="address/:id" component={Address as any} />
        </Route>
    </Router>
);
