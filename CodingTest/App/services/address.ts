
import { HttpClientAPI } from '../components/httpClientProvider';
import { Address } from '../model/address';

export function getAll(api: HttpClientAPI) {
    return api.http<any[]>('/api/address', { method: 'get'  });
}

export function load(api: HttpClientAPI, id: number) {
    return api.http<Address>(`/api/address/${id}`, { method: 'get' });
}

export function save(api: HttpClientAPI, data: Address) {
    return api.http<number>('/api/address', { method: 'post', body: JSON.stringify(data) });
}

export function remove(api: HttpClientAPI, id: number, rowVersion: number[]) {
    return api.http<void>(`/api/address/${id}?rowVersion=${rowVersion}`, { method: 'delete' });
}

export function exportToCsv(api: HttpClientAPI) {
    return location.href = '/api/address/Csv';
}