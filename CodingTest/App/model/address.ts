﻿export class Address {
    id: number;
    street: string;
    ward: string;
    district: string;
    city: string;
    country: string;
    lat: string;
    lng: string;
    rowVersion: number[];
}