﻿using CodingTest.Data;
using CodingTest.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Config;
using System.IO;

namespace CodingTest
{
    public class Startup
    {
        private static Logger logger;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);


            Configuration = builder.Build();

            var logConfig = new XmlLoggingConfiguration(Path.Combine(env.ContentRootPath, "NLog.config"));
            LogManager.Configuration = logConfig;
            logger = LogManager.GetLogger("CodingTest");
            logger.Info("Starting CodingTest.");

            AppDbConfig.ConnectionString = Configuration["Data:MainDb"];
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
              .AddScoped(provider => { return new AppDbContext(); });

            services.AddMvc(options =>
            {
                // This formats strings verbatim, which is not valid JSON, see https://github.com/aspnet/Mvc/issues/4146
                options.OutputFormatters.RemoveType<StringOutputFormatter>();
                // Nothing from the web API should be cached by browsers
                options.Filters.Add(new ResponseCacheFilter(new CacheProfile { NoStore = true }));
                // Turn various errors into specific http codes
                options.Filters.Add(new KnownExceptionsFilter());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                // All GET requests that do not match any previous routes are responded with our main html page.
                // This enables pushState navigation in IE11+ with deep linking.
                routes.MapSpaFallbackRoute("spa-fallback", new { controller = "Home", action = "Index" });
            });

            logger.Info("Startup completed.");
        }
    }
}
