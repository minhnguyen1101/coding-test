﻿namespace CodingTest.Model
{
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string Ward { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
