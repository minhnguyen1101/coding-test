/// <binding ProjectOpened='Watch - Development' />
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var extractCSS = new ExtractTextPlugin('site.css', { allChunks: true });
var isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
    resolve: {
        extensions: ['', '.js', '.jsx', '.ts', '.tsx', '.less'],
        modulesDirectories: [
            'node_modules',
            path.resolve(__dirname, './node_modules')
        ]
    },
    module: {
        loaders: [
            {
                test: /\.ts(x?)$/, include: /App/, enforce: 'pre', loader: 'tslint-loader', options: {
                    configuration: {
                        rules: {
                            quotemark: [true, 'single', 'jsx-single']
                        }
                    },

                    // can specify a custom config file relative to current directory or with absolute path
                    // 'tslint-custom.json'
                    configFile: false,

                    // tslint errors are displayed by default as warnings
                    // set emitErrors to true to display them as errors
                    emitErrors: false,

                    // tslint does not interrupt the compilation by default
                    // if you want any file with tslint errors to fail
                    // set failOnHint to true
                    failOnHint: true,

                    // enables type checked rules like 'for-in-array'
                    // uses tsconfig.json from current working directory
                    typeCheck: false,

                    // automatically fix linting errors
                    fix: false,

                    // can specify a custom tsconfig file relative to current directory or with absolute path
                    // to be used with type checked rules
                    tsConfigFile: 'tsconfig.json',
                    
                    // path to directory containing formatter (optional)
                    formattersDirectory: 'node_modules/tslint-loader/formatters/'                   
                }
            },
            { test: /\.ts(x?)$/, include: /App/, loader: 'babel-loader' },
            { test: /\.ts(x?)$/, include: /App/, loader: 'ts-loader?silent=true' },   
            { test: /(\.less|\.css)$/, loader: ExtractTextPlugin.extract('style', 'css!less') }
        ],
    },
    entry: {
        main: ['babel-polyfill', 'whatwg-fetch', './App/index.tsx'],
    },
    output: {
        path: path.join(__dirname, 'wwwroot', 'dist'),
        filename: '[name].js',
        publicPath: '/dist/'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(isDevelopment ? 'development:' : 'production')
            }
        }),
        extractCSS,
        new webpack.DllReferencePlugin({
            context: __dirname,
            manifest: require('./wwwroot/dist/vendor-manifest.json')
        })
    ].concat(isDevelopment ? [] : [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } })
    ])
};