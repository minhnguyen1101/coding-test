﻿using CodingTest.Data;
using CodingTest.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace CodingTest.Controllers
{
    [Route("api/[controller]")]
    public class AddressController : ApiController
    {
        public AddressController(AppDbContext db) : base(db)
        { }

        [HttpGet]
        public IEnumerable<Address> GetAll()
        {
            return db.Addresss;
        }

        [HttpGet("{id}")]
        public Address Load(int id)
        {
            return db.Addresss.Find(id);
        }

        [HttpPost]
        public int Save([FromBody]Address address)
        {
            try
            {
                using (var tx = db.Database.BeginTransaction(IsolationLevel.Serializable))
                {
                    db.Entry(address).State = address.Id == 0 ? EntityState.Added : EntityState.Modified;
                    db.SaveChanges();
                    tx.Commit();
                    return address.Id;
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        public void Remove(int id, byte[] rowVersion)
        {
            var address = db.Addresss.Find(id);
            if (address == null || !address.RowVersion.SequenceEqual(rowVersion))
                throw new DbUpdateConcurrencyException();

            db.Addresss.Remove(address);
            db.SaveChanges();
        }

        [HttpGet("CSV")]
        public FileResult ExportCsvStatements()
        {
            var addresses = db.Addresss.AsEnumerable();
            ActionContext.HttpContext.Response.ContentType = "text/csv";
            FileContentResult result = new FileContentResult(this.WriteDataToCsv(addresses), "application/pdf")
            {
                FileDownloadName = "Addresses.csv"
            };
            return result;
        }

        private byte[] WriteDataToCsv(IEnumerable<Address> addresses)
        {
            var csv = new StringBuilder();
            csv.AppendLine(string.Format("Generated on {0:dd/MM/yyyy HH:mm:ss}", DateTime.Now));
            csv.AppendLine("| Id | Street | Ward | District | City | Country |");
            csv.AppendLine("----------------------------------------------------------------");

            foreach (var address in addresses)
            {
                csv.AppendLine(string.Format("| {0} | {1} | {2} | {3} | {4} | {5} |",
                                              address.Id,
                                              address.Street,
                                              address.Ward,
                                              address.District,
                                              address.City,
                                              address.Country));
                csv.AppendLine("----------------------------------------------------------------");
            }

            byte[] buffer = Encoding.UTF8.GetBytes(csv.ToString());
            return buffer;
        }
    }
}
