﻿using System.Globalization;
using System.IO;
using CodingTest.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace CodingTest.Controllers
{
    // Does not inherit our own ApiController, as this is just a dummy controller to serve the home HTML page
    // Note: doing so excludes this controller from client-side code gen.
    public class HomeController : Controller
    {
        private static readonly string cacheDate = GetCacheDate();

        public HomeController()
        {
        }

        [AddHeader("X-Frame-Options", "deny")]
        [AddHeader("X-XSS-Protection", "1; mode=block")]
        public IActionResult Index()
        {
            return View(new Model { CacheDate = cacheDate, });
        }

        private static string GetCacheDate()
        {
            string assemblyFile = typeof(HomeController).Assembly.Location;
            return new FileInfo(assemblyFile).LastWriteTime.ToString("yyyy-MM-dd@HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public class Model
        {
            public string CacheDate { get; set; }
        }
    }
}