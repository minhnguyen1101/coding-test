﻿using CodingTest.Data;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace CodingTest.Controllers
{
    public abstract class ApiController
    {
        protected static Logger logger = LogManager.GetLogger("CodingTest");
        protected readonly AppDbContext db;

        [ActionContext]
        public ActionContext ActionContext { get; set; }

        public ApiController(AppDbContext db)
        {
            this.db = db;
        }
    }
}
