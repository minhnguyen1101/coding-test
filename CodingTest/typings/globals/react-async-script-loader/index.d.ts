declare module 'react-async-script-loader' {
    import { Component } from 'react';

    class ElementClass extends Component<any, any> { }

    interface ClassDecorator {
        (component: ElementClass): ElementClass;
    }

    function scriptLoader(url: string | string[]): ClassDecorator;

    export default scriptLoader;
}