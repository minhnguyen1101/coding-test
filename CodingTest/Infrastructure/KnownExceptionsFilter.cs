﻿using System;
using System.Data.Entity.Infrastructure;
using System.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;

namespace CodingTest.Infrastructure
{
    public class KnownExceptionsFilter : ExceptionFilterAttribute
    {
        private static Logger logger = LogManager.GetLogger("CodingTest");

        public override void OnException(ExceptionContext context)
        {
            var ex = context.Exception;
            var request = context.HttpContext.Request;

            if (ExceptionToHttpResult(ex, context))
                logger.Warn(ex);
            else
                logger.Error(ex);
        }

        private bool ExceptionToHttpResult(Exception ex, ExceptionContext context)
        {
            if (ex is DbUpdateConcurrencyException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status412PreconditionFailed);
                return true;
            }

            if (ex is BusinessException)
            {
                context.Result = new ObjectResult(new { ex.Message, (ex as BusinessException).Infos }) { StatusCode = StatusCodes.Status406NotAcceptable };
                return true;
            }

            if (ex is SecurityException)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
                return true;
            }

            if (ex is ValidateException)
            {
                context.Result = new ObjectResult(new { ex.Message }) { StatusCode = StatusCodes.Status400BadRequest };
                return true;
            }

            return false;
        }
    }

    public class BusinessException : Exception
    {
        public object Infos { get; set; }

        public BusinessException(string message) : base(message)
        { }
    }

    public class ValidateException : Exception
    {
        public ValidateException(string message) : base(message)
        { }
    }

    public class ValidateUniqueException : ValidateException
    {
        public ValidateUniqueException(string message) : base(message)
        { }
    }
}
